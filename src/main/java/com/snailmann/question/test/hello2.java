package com.snailmann.question.test;


import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Data
@Component("hello2")
public class hello2 implements Hello {

    String name = "hello2";

    public hello2() {
        log.warn("hello2 constructor");
    }

}
